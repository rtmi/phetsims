// Copyright 2013-2021, University of Colorado Boulder

/**
 * MagnetsControlPanel is a panel that contains controls for magnets.
 *
 * @author Chris Malley (PixelZoom, Inc.)
 * @author Sam Reid (PhET Interactive Simulations)
 * @author Steele Dalton (PhET Interactive Simulations)
 */

import merge from '../../../../phet-core/js/merge.js';
import PhetFont from '../../../../scenery-phet/js/PhetFont.js';
import { Text } from '../../../../scenery/js/imports.js';
import { VBox } from '../../../../scenery/js/imports.js';
import RectangularPushButton from '../../../../sun/js/buttons/RectangularPushButton.js';
import Panel from '../../../../sun/js/Panel.js';
import exampleSim from '../../exampleSim.js';
import exampleSimStrings from '../../exampleSimStrings.js';
import Vector2 from '../../../../dot/js/Vector2.js';
import dotRandom from '../../../../dot/js/dotRandom.js';
import ExampleSimConstants from '../../common/ExampleSimConstants.js';
import ModelViewTransform2 from '../../../../phetcommon/js/view/ModelViewTransform2.js';

class MagnetsControlPanel extends Panel {

  /**
   * @param {MagnetsModel} model - the model for the entire screen
   * @param {Object} [options] - options for the control panel, see Panel.js for options
   */
  constructor( model, options ) {

    // Demonstrate a common pattern for specifying options and providing default values
    options = merge( {

      // Panel options
      xMargin: 10,
      yMargin: 10,
      stroke: 'orange',
      lineWidth: 3
    }, options );

    // 'Magnet Controls' title
    const magnetControlsTitleNode = new Text( exampleSimStrings.magnetControls, {
      font: new PhetFont( {
        size: 18,
        weight: 'bold'
      } )
    } );

    // 'Flip Polarity' button
    const flipPolarityButton = new RectangularPushButton( {
      content: new Text( exampleSimStrings.flipPolarity, {
        font: new PhetFont( 16 )
      } ),
      baseColor: 'yellow',
      xMargin: 10,
      listener: () => {
        const orientation = model.barMagnet.orientationProperty.get() + Math.PI;
        model.barMagnet.orientationProperty.set( orientation );
      }
    } );

    // 'Move Magnet' button
    const moveMagnetButton = new RectangularPushButton( {
      content: new Text( exampleSimStrings.moveMagnet, {
        font: new PhetFont( 16 )
      } ),
      baseColor: 'yellow',
      xMargin: 10,
      listener: () => {
        // make a randomized position then move the magnet
        const vec = randomizePosition( options.visibleBounds );
        model.barMagnet.positionProperty.set( vec );
      }
    } );

    // 'Add Magnet' button
    const addMagnetButton = new RectangularPushButton( {
      content: new Text( exampleSimStrings.addMagnet, {
        font: new PhetFont( 16 )
      } ),
      baseColor: 'yellow',
      xMargin: 10,
      listener: () => {
        // make a randomized position then place the magnet on the screen
        const vec = randomizePosition( options.visibleBounds );
        options.extraAdder( vec );
      }
    } );

    // The contents of the control panel
    const content = new VBox( {
      align: 'center',
      spacing: 10,
      children: [
        magnetControlsTitleNode,
        flipPolarityButton,
        moveMagnetButton,
        addMagnetButton
      ]
    } );

    super( content, options );
  }
}

// make a randomized position within the viewport
function randomizePosition( bounds ) {
  // calc south-east corner of the viewport
  const maxX = bounds.right - ExampleSimConstants.SCREEN_VIEW_X_MARGIN;
  const maxY = bounds.bottom - ExampleSimConstants.SCREEN_VIEW_Y_MARGIN;
  // get random number between 0 and the max X/Y values
  let rx = dotRandom.nextInt( maxX );
  let ry = dotRandom.nextInt( maxY );
  // shift x to right of (left edge) margin
  if ( rx < ExampleSimConstants.SCREEN_VIEW_X_MARGIN ) {
    rx = rx + ExampleSimConstants.SCREEN_VIEW_X_MARGIN;
  }
  // shift y below (top edge) margin
  if ( ry < ExampleSimConstants.SCREEN_VIEW_Y_MARGIN ) {
    ry = ry + ExampleSimConstants.SCREEN_VIEW_Y_MARGIN;
  }

  // transform from view to model coordinates 
  const center = new Vector2( bounds.width / 2, bounds.height / 2 );
  const mvt = ModelViewTransform2.createOffsetScaleMapping( center, 1 );
  const vec = new Vector2( rx, ry );
  return mvt.viewToModelPosition( vec );
}

exampleSim.register( 'MagnetsControlPanel', MagnetsControlPanel );
export default MagnetsControlPanel;
